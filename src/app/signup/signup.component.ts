import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms"
import { Router } from '@angular/router';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public signupForm !: FormGroup;
  private signupUrl = "http://localhost:3000/api/v1/users/add";
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      fullName:[''],
      email:[''],
      password:[''],
      mobile: ['']
    });
  }

  signUp() {
    this.http.post<any>(this.signupUrl, this.signupForm.value)
    .subscribe(res => {
      alert('Sign Up Successful');
      this.signupForm.reset();
      this.router.navigate(['login']);
    }, err => {
      alert('Sign Up Failed with error: ' + err.message);
    });
  }

}
