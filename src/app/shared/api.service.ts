import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public apiUrl = "http://localhost:3000/api/v1"
  constructor(private http: HttpClient) { }

  postUser(data: any) {
    return this.http.post<any>(this.apiUrl+ '/post', data)
    .pipe(map((res: any) =>{
      return res;
    } ));
  }
}
