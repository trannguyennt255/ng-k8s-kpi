export class UserModel {
    id: number;
    fullName: string = '';
    email: string = '';
    mobile: number;
    salary: number;
}