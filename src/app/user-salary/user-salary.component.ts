import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-user-salary',
  templateUrl: './user-salary.component.html',
  styleUrls: ['./user-salary.component.css']
})
export class UserSalaryComponent implements OnInit {
  public userInfo: UserModel = null;
  public loginDeets: any;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.getUserInfo();
    this.http.get<any>(`http://localhost:3000/api/v1/users/${this.loginDeets._id}`)
      .subscribe(res => {
        this.userInfo = res.data.user;
      }, err => {
        alert('Error: ' + err.message);
      });
  }

  getUserInfo() {
    if (localStorage.getItem('currentUser')) {
      this.loginDeets = JSON.parse(localStorage.getItem('currentUser'))
    }
  }
}
