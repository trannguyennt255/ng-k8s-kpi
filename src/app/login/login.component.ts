import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup} from "@angular/forms";
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm!: FormGroup;
  private loginUrl = "http://localhost:3000/api/v1/users/login"
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email:[''],
      password:['']
      });
  }

  login() {
    this.http.post<any>(this.loginUrl, this.loginForm.value)
    .subscribe(res => {
      alert('Login Successful');
      localStorage.setItem('currentUser', JSON.stringify(res.data));
      this.router.navigate(['user-salary']);
    }, err => {
      alert('Login Failed');
    });
  }

}
